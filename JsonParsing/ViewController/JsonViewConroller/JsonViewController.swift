//
//  JsonViewController.swift
//  TestApps
//

import UIKit

class JsonViewController: UIViewController {
    
    @IBOutlet weak var listSearchBar: UISearchBar!
    @IBOutlet weak var dataTableView: UITableView!
    
    var allPosterInfo: allResult?
    var isFilterStart = false
    
    
    
    var filterMovies = [dataModelInfo]()
    var filterMovie = dataModelInfo(original_title: " ", overview: " ")
    var loadedFilterMovies = [dataModelInfo]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //filterMovie =
        
        dataTableView.register(UINib(nibName: "DataTableViewCell", bundle: nil), forCellReuseIdentifier: "DataTableViewCell")
        dataTableView.dataSource = self
        dataTableView.delegate = self
        
        dataTableView.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 0, right: 0)
        listSearchBar.delegate = self
        fetchData()
        
    }
    
    
    
    func fetchData(){
        
        getPosterInfoFromAPI {[self] isFinishedFetch in
            
            
            DispatchQueue.main.async {
                
                if allPosterInfo != nil{
                    for i in 0..<(self.allPosterInfo!.results.count){
                        
                        
                        if let postPath =  self.allPosterInfo?.results[i].poster_path{
                            print(" poster Path::  ", postPath)
                        }
                        
                        if let title = allPosterInfo?.results[i].original_title{
                            
                            
                            print("tite:: ", title)
                            
                            filterMovie.original_title = title
                        }else{
                            filterMovie.original_title = "nil"
                        }
                        
                        //  if let
                        
                        self.filterMovies.append(filterMovie)
                        //self.filterMovies.append(dataModelInfo(original_title: self.allPosterInfo!.results[i].original_title!, overview: self.allPosterInfo!.results[i].overview!, poster_image: nil))
                        
                    }
                }
                self.dataTableView.reloadData()
            }
        }
        
    }
    func getPosterInfoFromAPI(completion: @escaping  (Bool?) -> () ){
        
        let urlStr = "https://api.themoviedb.org/3/search/movie?api_key=38e61227f85671163c275f9bd95a8803&query=marvel"
        
        let session = URLSession.shared
        
        
        guard let url = URL(string: urlStr) else {
            return
        }
        session.dataTask(with: url) { (data, response, err) in
            
            guard let data = data else{return}
            
            print(data.base64EncodedData())
            do{
                
                let json  =  try JSONDecoder().decode(allResult.self, from: data)
                
                self.allPosterInfo = json
                completion(true)
                
            }
            catch let jeerr
            {
                print(jeerr.localizedDescription)
            }
        }.resume()
        
    }
    
    deinit{
        
        print("Deinit called.. memory released")
    }
    
}

//MARK: - TableView Delegate method

extension JsonViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headView = UIView()
        let headTxt = "Movie List"
        let lbl = UILabel()
        lbl.text = headTxt
        lbl.font = lbl.font.withSize(40)
        lbl.frame = CGRect(x: 20, y: 0, width: 200, height: 40)
        
        headView.addSubview(lbl)
        
        headView.frame = CGRect(x: 0, y: 0, width: 400, height: 40)
        //  headView.backgroundColor = .cyan
        return headView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

//MARK: - TableView DataSource method

extension JsonViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isFilterStart{
            return loadedFilterMovies.count
        }
        
        
        if let count =  allPosterInfo?.results.count{
            return count
        }
        
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = dataTableView.dequeueReusableCell(withIdentifier: "DataTableViewCell", for: indexPath) as! DataTableViewCell
        
        if isFilterStart{
            
            cell.itmsLbl.text = loadedFilterMovies[indexPath.row].original_title
            cell.overviewTextView.text = loadedFilterMovies[indexPath.row].overview
            print("text Height:: ",cell.overviewTextView.contentSize.height)
            
        }else{
            
            cell.itmsLbl.text = allPosterInfo?.results[indexPath.row].original_title//items[indexPath.row]
            cell.overviewTextView.text = allPosterInfo?.results[indexPath.row].overview
            
        }
        
        
        
        return cell
        
    }
}


//MARK: - Search bar delegate method

extension JsonViewController: UISearchBarDelegate{
    
    
    //MARK:- Search bar delegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        
        if searchBar.text == ""{
            isFilterStart = false
            dataTableView.reloadData()
        }else{
            
            isFilterStart = true
            loadedFilterMovies =  filterMovies.filter({ user -> Bool in
                return  user.original_title.lowercased().contains(searchText.lowercased())
            })
            dataTableView.reloadData()
        }
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isFilterStart = false
        dataTableView.reloadData()
        
        
        
        
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        // isFilterStart = true
        //dataTableView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        isFilterStart = false
        dataTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        //  dismissKeyboard()
        
        
        isFilterStart = false
        dataTableView.reloadData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
}
