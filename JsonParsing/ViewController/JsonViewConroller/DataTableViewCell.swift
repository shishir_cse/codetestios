//
//  DataTableViewCell.swift
//  TestApps
//

import UIKit

class DataTableViewCell: UITableViewCell {

    @IBOutlet weak var overviewTextView: UITextView!
    @IBOutlet weak var itmsLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
