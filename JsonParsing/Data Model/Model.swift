//
//  Model.swift
//  TestApps
// Md Saddam Hossain

import Foundation
import UIKit

struct allResult: Decodable{
    var page: Int
    var results: [myResult]
    var total_pages: Int
    var total_results: Int
    
    
}


struct myResult: Decodable{
    
    var original_title: String?
    var overview: String?
    var poster_path: String?
    
}


struct dataModelInfo{
    
    var original_title: String
    var overview: String
    var poster_image: UIImage?
    
}
